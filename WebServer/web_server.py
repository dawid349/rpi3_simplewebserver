'''
	Raspberry Pi GPIO Status and Control
'''
import RPi.GPIO as GPIO
from flask import Flask, render_template, request
app = Flask(__name__)
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

# define relays GPIOs
relay1 = 13
relay2 = 19
relay3 = 26
relay4 = 6
# initialize relays status variables
relay1Sts = 0
relay2Sts = 0
relay3Sts = 0
relay4Sts = 0

# define relays control pins as outputs
GPIO.setup(relay1, GPIO.OUT)
GPIO.setup(relay2, GPIO.OUT)
GPIO.setup(relay3, GPIO.OUT)
GPIO.setup(relay4, GPIO.OUT)
# turn relays OFF
GPIO.output(relay1, GPIO.LOW)
GPIO.output(relay2, GPIO.LOW)
GPIO.output(relay3, GPIO.LOW)
GPIO.output(relay4, GPIO.LOW)


@app.route("/")
def index():
    # read relays Status
    relay1Sts = GPIO.input(relay1)
    relay2Sts = GPIO.input(relay2)
    relay3Sts = GPIO.input(relay3)
    relay4Sts = GPIO.input(relay4)

    templateData = {
        'relay1': relay1Sts,
        'relay2': relay2Sts,
        'relay3': relay3Sts,
        'relay4': relay4Sts,
    }
    return render_template('index.html', **templateData)


@app.route("/<deviceName>/<action>")
def action(deviceName, action):
    if deviceName == 'relay1':
        actuator = relay1
    if deviceName == 'relay2':
        actuator = relay2
    if deviceName == 'relay3':
        actuator = relay3
    if deviceName == 'relay4':
        actuator = relay4

    if action == "on":
        GPIO.output(actuator, GPIO.HIGH)
    if action == "off":
        GPIO.output(actuator, GPIO.LOW)

    relay1Sts = GPIO.input(relay1)
    relay2Sts = GPIO.input(relay2)
    relay3Sts = GPIO.input(relay3)
    relay4Sts = GPIO.input(relay4)

    templateData = {
        'relay1': relay1Sts,
        'relay2': relay2Sts,
        'relay3': relay3Sts,
        'relay4': relay4Sts,
    }

    return render_template('index.html', **templateData)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug=True)
